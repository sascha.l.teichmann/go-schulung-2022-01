package main

import "fmt"

type MeinInterface interface {
	Get() int
}

type A int
type B float32

func (a A) Get() int { return 42 }
func (b B) Get() int { return 67 }

func myPrint(x interface{}) {
}

func work(mi MeinInterface) {

	// x := mi.(A) -> crash

	if a, ok := mi.(A); ok { // Type assertion
		a += 32

		fmt.Printf("%T\n", mi)
		fmt.Printf("%T: %d\n", a, a)
	}

	switch x := mi.(type) {
	case A:
		fmt.Printf("A: %d\n", x)
	case B:
		fmt.Printf("B: %f\n", x)
	}

	_ = mi.Get()
}

func main() {

	var a A = 42
	var b B = 3.1415

	work(a)
	work(b)
}

package main

import (
	"fmt"
	"log"
	"regexp"
	"strconv"
)

var parseRe = regexp.MustCompile("(.)")

type myError int

func (me myError) Error() string {
	return strconv.Itoa(int(me))
}

func deep(n int) {
	if n == 0 {
		//return
		panic("too deep")
	}
	deep(n - 1)
}

func something(a int, b float64) (v int, err error) {

	defer func() {
		fmt.Println("Hello from defer1")
	}()

	defer func() {
		if x := recover(); x != nil {
			err = myError(53)
			fmt.Println("Hello from defer2", x)
		}
	}()

	deep(10)

	if a == 42 {
		return 0, myError(42)
	}

	return a * int(b), nil
}

func main() {

	// err-lang <- go-lang

	if v, err := something(42, 3.145); err != nil {
		if x, ok := err.(myError); ok {
			log.Fatalf("error: %d\n", int(x))
		}
	} else {
		fmt.Println(v)
	}
}

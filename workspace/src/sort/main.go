package main

import (
	"fmt"
	"sort"
)

type sortable []string

func (s sortable) Len() int           { return len(s) }
func (s sortable) Less(i, j int) bool { return s[i] > s[j] }
func (s sortable) Swap(i, j int)      { s[i], s[j] = s[j], s[i] }

type person struct {
	alter int
	name  string
}

type persons []person

func (p persons) Len() int      { return len(p) }
func (p persons) Swap(i, j int) { p[i], p[j] = p[j], p[i] }

//func (p persons) Less(i, j int) bool { return p[i].alter < p[j].alter }

type AgeSort struct{ persons }

func (as AgeSort) Less(i, j int) bool {
	return as.persons[i].alter < as.persons[j].alter
}

type NameSort struct{ persons }

func (ns NameSort) Less(i, j int) bool {
	return ns.persons[i].name < ns.persons[j].name
}

func main() {

	personen := []person{
		{86, "Erna"},
		{21, "Kevin"},
	}

	/*
		sort.Slice(personen, func(i, j int) bool {
			return personen[i].alter < personen[j].alter
		})
	*/

	data := []string{
		"Peter",
		"Alex",
		"Karl",
		"Manuela",
		"Hans",
	}

	// sort.Strings(data)

	/*
		sort.Slice(data, func(x, y int) bool {
			return data[x] < data[y]
		})
	*/

	s := sortable(data)

	sort.Sort(s)

	fmt.Printf("%v\n", data)

	sort.Sort(AgeSort{persons(personen)})
	fmt.Printf("%v\n", personen)
	sort.Sort(NameSort{persons(personen)})
	fmt.Printf("%v\n", personen)
}

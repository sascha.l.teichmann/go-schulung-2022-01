package main

import "fmt"

func main() {

	z := [...]int{
		0: 1, 2: 3,
	}
	_ = z

	var x [3]int

	x[0] = 42
	x[1] = 43
	x[2] = 44
	//x[4] = 44

	var y [3]int

	x = y

	fmt.Println(x)

	for i := range [100_000]struct{}{} {
		fmt.Println(i)
	}

}

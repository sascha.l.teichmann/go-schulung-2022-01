package main

import "fmt"

func modify(x *int) {
	*x = 42
}

func newFooBar() *int {
	var local int
	local = 13
	return &local
}

func main() {

	//var tmp int
	//y = &tmp

	y := new(int)

	var x int

	modify(&x)

	var p *int

	p = &x

	fmt.Println(*p)
	fmt.Println(*newFooBar())
}

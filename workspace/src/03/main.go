package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func foo(fn func(int, int) int) {
}

func main() {

	sc := bufio.NewScanner(os.Stdin)

	//fmt.Fscanf(os.Stdin, "%d", &x)

	for sc.Scan() {
		line := sc.Text()
		if line == "" {
			fmt.Println("Eingabe bitte")
			continue
		}
		x, err := strconv.Atoi(line)
		if err != nil {
			fmt.Printf("bäh: %v\n", err)
			continue
		}
		fmt.Println(x)
	}

	//fmt.Printf("%d\n", res)
}

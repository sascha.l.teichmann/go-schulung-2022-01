package main

import "fmt"

func direct() int {
	sum := 0
	for i := 1; i <= 10; i++ {
		sum += i
	}
	return sum
}

func sum(from, to int) int {
	sum := 0
	for i := from; i <= to; i++ {
		sum += i
	}
	return sum
}

func operate(from, to int, op func(int)) {
	for i := from; i <= to; i++ {
		op(i)
	}
}

func add(from, to int) int {
	sum := 0
	operate(from, to, func(x int) { sum += x })
	return sum
}

func mul(from, to int) int {
	prod := 1
	operate(from, to, func(x int) { prod *= x })
	return prod
}

func iter(from, to int) func() (int, bool) {
	return func() (int, bool) {
		x := from
		from++
		return x, from <= to+1
	}
}

func addOp() (func(int), func() int) {
	sum := 0
	return func(x int) { sum += x }, func() int { return sum }
}

func mulOp() (func(int), func() int) {
	prod := 1
	return func(x int) { prod *= x }, func() int { return prod }
}

func op(it func() (int, bool), fn func() (func(int), func() int)) int {
	o, r := fn()
	for i, ok := it(); ok; i, ok = it() {
		o(i)
	}
	return r()
}

func main() {
	fmt.Printf("direct: %d\n", direct())
	fmt.Printf("sum: %d\n", sum(1, 10))
	fmt.Printf("add: %d\n", add(1, 10))
	fmt.Printf("mul: %d\n", mul(1, 10))
	fmt.Printf("it add: %d\n", op(iter(1, 10), addOp))
	fmt.Printf("it mul: %d\n", op(iter(1, 10), mulOp))
}

package main

import "fmt"

func main() {

	//var punkte map[string]int

	//punkte := make(map[string]int)
	punkte := map[string]int{
		"Klaus": 12,
		"Otto":  0,
	}

	punkte["Peter"] = 42

	fmt.Println(punkte)
	fmt.Println(punkte["Peter"])
	fmt.Println(punkte["Otto"])
	fmt.Println(punkte["Hermann"])

	//punkte["Hermann"]++
	//_ = x

	if v, found := punkte["Hermann"]; found {
		fmt.Printf("Punkte %d\n", v)
	}

	for range punkte {
		fmt.Printf("%s\n", "xxx")
	}
}

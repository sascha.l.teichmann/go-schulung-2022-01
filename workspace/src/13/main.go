package main

import "fmt"

type Aging interface {
	Age() int
}

type Greis int
type Tween int

func (g Greis) Age() int { return 89 }
func (j Tween) Age() int { return 22 }

func showAge(a Aging) {
	fmt.Println(a.Age())
}

func main() {

	//var x Aging

	var g Greis
	var t Tween

	showAge(g)
	showAge(t)
}

package main

import "fmt"

type MeinTyp int

func (mt MeinTyp) Print() {
	mt = 32
	fmt.Println(mt)
}

func (mt MeinTyp) Singe() {
	fmt.Println("lalal", mt)
}

func (mt *MeinTyp) Add(value int) {
	*mt += MeinTyp(value)
}

func main() {

	//var mt MeinTyp

	y := uint32(42)

	x := MeinTyp(y)

	x += 3

	x.Print()
	x.Add(60)
	x.Singe()
}

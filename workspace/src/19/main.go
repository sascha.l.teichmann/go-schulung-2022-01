package main

import (
	"fmt"
)

func main() {

	fmt.Println("Halo")
	fmt.Println("Halo")
	fmt.Println("Halo")
	fmt.Println("Halo")

	//var ch chan bool
	done := make(chan struct{})

	job := make(chan int)

	go func() {
		defer close(done)

		fmt.Println("Hello from Go routine")

		for x := range job {
			fmt.Printf("x: %d\n", x)
		}
	}()

	job <- 32
	job <- 78
	job <- 78
	job <- 78
	job <- 78
	close(job)

	_, ok := <-done
	fmt.Println("Go routine done", ok)

	//time.Sleep(time.Second)
}

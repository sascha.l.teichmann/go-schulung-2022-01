package main

import "fmt"

func main() {
	var slice []int

	if slice == nil {
		fmt.Println("nit init")
	}
	fmt.Println(len(slice))

	var arr [3]int

	slice = arr[:2]
	fmt.Println(len(slice))
	fmt.Println(cap(slice))
	slice = slice[:len(slice)+1]
	fmt.Println(len(slice))

	arr[1] = 42
	slice[0] = 32

	fmt.Println(slice)
	fmt.Println(arr)

	slice2 := make([]int, 23)
	fmt.Println(slice2)

	slice2 = append(slice2, 3, 3, 20)
	fmt.Println(slice2)
	fmt.Println(cap(slice2))
}

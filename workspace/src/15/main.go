package main

import "fmt"

type Auto interface {
	Starten()
	Bremsen()
}

func fahren(a Auto) {
	a.Starten()
	a.Bremsen()
}

type Bremse int
type Motor int

func (b Bremse) Bremsen() { fmt.Println("bremsen") }
func (m Motor) Starten()  { fmt.Println("starten") }

func main() {

	var m Motor
	var b Bremse

	var a Auto = struct {
		Motor
		Bremse
	}{m, b}

	fahren(a)
}

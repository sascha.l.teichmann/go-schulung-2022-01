package main

import "fmt"

type MeinTyp int
type IhrTyp int

type DeinTyp struct {
	MeinTyp
	IhrTyp
}

func (it IhrTyp) Print() {
	fmt.Println("Print from IhrTyp")
}

func (mt MeinTyp) Print() {
	fmt.Println(mt)
}

func (it IhrTyp) Hello() {
	fmt.Println("Hello")
}

/**
func (dt DeinTyp) Print() {
	dt.MeinTyp.Print()
	dt.IhrTyp.Print()
	fmt.Println("Print ftom DeinTyp")
}
*/

func main() {
	c := DeinTyp{}
	c.IhrTyp.Print()
	c.Hello()
}

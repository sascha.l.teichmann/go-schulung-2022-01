package main

import (
	"database/sql"
	"fmt"
)

type MyDB interface {
	Begin() (*sql.Tx, error)
	Close() error
}

func meineAlgo(mdb MyDB) {
	tx, _ := mdb.Begin()
	_, _ = tx, err
	mdb.Close()
}

type FakeDB struct {
	*sql.DB
}

func (fdb FakeDB) Begin() (*sql.Tx, error) {
	fmt.Println("Begin called")
	return fdb.DB.Begin()
}

func main() {

	db, _ := sql.Open("postgres", "")

	f := FakeDB{db}

	meineAlgo(f)

}

package main

import "fmt"

type (
	Ort struct {
		strasse string `json:"strasse"`
		hausNr  int
		_       [4]byte
	}

	Person struct {
		name  string
		alter int
		ort   Ort
	}
)

func main() {

	var person = Person{
		name: `Peter" 
MehrZeiler
Lustig`,
		ort: Ort{
			strasse: "Dadortweg",
			hausNr:  42,
		},
	}

	person.alter = 56
	person.ort.hausNr++

	fmt.Printf("%v\n", person)
}

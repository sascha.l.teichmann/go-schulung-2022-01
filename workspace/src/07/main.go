package main

import "fmt"

func gotcha(slice *[]int) {
	fmt.Println(*slice)
	*slice = append(*slice, 5)
	(*slice)[0] = 9
	fmt.Println(*slice)
}

func main() {

	slice := []int{1, 2, 3, 4}
	gotcha(&slice)
	fmt.Println(slice)

	/*
		sl2 := make([][100_000]byte, 0, 1000)
		sl2 = sl2[:len(sl2)-1]
		sl2 = sl2[:len(sl2)+1]
	*/
}

package main

import "fmt"

func foo() bool {
	return true
}

func main() {

	cond2 := false

	if cond := foo(); cond {
	} else if cond2 {
	} else {
	}

	switch x := 10; x {
	case 10:
		fmt.Println("ten")
		fallthrough
	case 20:
		fmt.Println("twenty")
	case 30, 40, 50:
		fmt.Println("liste")
	default:
		fmt.Println("default")
	}

	switch {
	case x*2 > 1000:
	case y*2 > 1000:
	}

label:

	goto label
}

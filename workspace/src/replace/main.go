package main

import (
	"io"
	"os"
)

type bigA struct{ io.Reader }

func (ba bigA) Read(b []byte) (int, error) {
	n, err := ba.Reader.Read(b)
	for i, x := range b[:n] {
		if x == 'a' {
			b[i] = 'A'
		}
	}
	return n, err
}

func main() {
	//in := strings.NewReader("Hallo\n")
	ba := bigA{os.Stdin}
	io.Copy(os.Stdout, ba)
}

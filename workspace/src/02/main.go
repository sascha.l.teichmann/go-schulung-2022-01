package main

import "fmt"

func foo(x int) {
	x *= 2
	x++
	x = x + 1
	x += 1
	fmt.Printf("%d\n", x)
}

func bar(fn func(int) int) {
	fmt.Printf("From bar\n")
	x := fn(42)
	fmt.Printf("From bar: %d\n", x)
}

func baz(x int) int {
	return x * 2
}

func boo() int {
	return 42
}

func construct(x int) func() int {
	return func() int {
		return x
	}
}

func main() {
	x := 42
	foo(x)
	fmt.Println(x)

	//var fn func()
	// fn = foo
	fn := foo
	fn(32)
	bar(baz)

	fx := construct(56)
	fy := construct(677)
	fmt.Printf("fx: %d\n", fx())
	fmt.Printf("fx: %d\n", fx())
	fmt.Printf("fx: %d\n", fy())
	fmt.Printf("fx: %d\n", construct(56))
}
